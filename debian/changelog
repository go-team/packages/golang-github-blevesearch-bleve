golang-github-blevesearch-bleve (0.5.0+git20170912.278.6eea5b78-6) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on golang-github-spf13-cobra-dev.
    + golang-github-blevesearch-bleve-dev: Drop versioned constraint on
      golang-github-spf13-cobra-dev in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 25 May 2022 11:26:53 +0100

golang-github-blevesearch-bleve (0.5.0+git20170912.278.6eea5b78-5) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Update standards version, no changes needed.
  * Change priority extra to priority optional.
  * Update renamed lintian tag names in lintian overrides.
  * Set upstream metadata fields: Name.
  * Bump debhelper from old 10 to 12.
  * Set upstream metadata fields: Repository.
  * Remove obsolete fields Name from debian/upstream/metadata.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 01 Feb 2021 22:52:29 +0000

golang-github-blevesearch-bleve (0.5.0+git20170912.278.6eea5b78-4) unstable; urgency=medium

  [ Christos Trochalakis ]
  * QA upload.
  * Depend on golang-github-rcrowley-go-metrics-dev.

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

 -- Christos Trochalakis <ctrochalakis@debian.org>  Wed, 11 Apr 2018 15:32:26 +0300

golang-github-blevesearch-bleve (0.5.0+git20170912.278.6eea5b78-3) unstable; urgency=medium

  * Remove Michael Lustfield from Uploaders list.
  * Orphaning package.

 -- Michael Lustfield <michael@lustfield.net>  Sat, 03 Feb 2018 12:13:26 -0600

golang-github-blevesearch-bleve (0.5.0+git20170912.278.6eea5b78-2) unstable; urgency=medium

  * Remove produced binaries from build; no current need.

 -- Michael Lustfield <michael@lustfield.net>  Wed, 04 Oct 2017 22:45:35 -0500

golang-github-blevesearch-bleve (0.5.0+git20170912.278.6eea5b78-1) unstable; urgency=medium

  * New upstream revision.
  * Fix/enable unit tests; skip unsupported tests.
  * Maintenance:
    - Update for standards version 4.1.0.0.
    - Correct d/watch.
    - Add Testsuite to d/control.

 -- Michael Lustfield <michael@lustfield.net>  Mon, 02 Oct 2017 15:21:40 -0500

golang-github-blevesearch-bleve (0.5.0+git20170628.269.174f8ed4-2) unstable; urgency=medium

  * Added patch to support tests.
  * Removed test override.

 -- Michael Lustfield <michael@lustfield.net>  Mon, 24 Jul 2017 19:19:33 -0500

golang-github-blevesearch-bleve (0.5.0+git20170628.269.174f8ed4-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Lustfield <michael@lustfield.net>  Mon, 17 Jul 2017 16:05:57 -0500

golang-github-blevesearch-bleve (0.5.0+git20170324.202.4702785f-2) unstable; urgency=medium

  * Update dependency from golang-goleveldb-dev to
    golang-github-syndtr-goleveldb-dev.

 -- Steve Langasek <vorlon@debian.org>  Sun, 25 Jun 2017 11:56:28 -0700

golang-github-blevesearch-bleve (0.5.0+git20170324.202.4702785f-1) unstable; urgency=medium

  * Initial release (Closes: #855678)

 -- Michael Lustfield <michael@lustfield.net>  Thu, 30 Mar 2017 16:06:03 -0500
